using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Umbraco.Web.WebApi;
using Umbraco.Core.Composing;
using NPoco;
using System.Net.Configuration;
using System.Configuration;
using System.Net.Mail;
using System.Web;
using SundownBoulevard.Core.Models;
using SundownBoulevard.Core.Helpers;

namespace SundownBoulevard.Core.Controllers.api
{
    [Route("umbraco/api/[controller]")]
    public class BookingController : UmbracoApiController
    {
        // GET: Booking
        [HttpGet]
        public int AddBooking(string Confirmed, DateTime BookingDate, DateTime BookingTime, int BookingSize)
        {
            SunTableModel Booking = new SunTableModel();
            Booking.Confirmed = Confirmed;
            Booking.Email = "";
            Booking.Name = "";
            Booking.BookingSize = BookingSize;
            Booking.BookingDate = BookingDate;
            Booking.BookingTime = BookingTime;
            Booking.FoodName = "";
            Booking.DrinkName = "";

            using (var scope = Current.ScopeProvider.CreateScope())
            {
                var database = scope.Database;
                scope.Database.Insert<SunTableModel>(Booking);
                int id = database.Query<int>("SELECT TOP 1 id from Bookings ORDER BY id DESC").FirstOrDefault();
                scope.Complete();
                return id;
            }
        }
        [HttpGet]
        public bool CompleteBooking(int bookingId, string name, string email, string drinkName, string foodName, int guestPageId)
        {
            using (var scope = Current.ScopeProvider.CreateScope())
            {
                IDatabase db = new Database(scope.Database.Connection);
                // Get specific booking by id;
                var booking = db.SingleById<SunTableModel>(bookingId);
                booking.Confirmed = "true";
                booking.Name = name;
                booking.Email = email;
                booking.DrinkName = drinkName;
                booking.FoodName = foodName;
                db.Update(booking);
                scope.Complete();
                return SendConfirmMail(email, guestPageId);
            }
        }
        [HttpGet]
        public bool DeleteBooking(int bookingId)
        {
            using (var scope = Current.ScopeProvider.CreateScope())
            {
                IDatabase db = new Database(scope.Database.Connection);
                var bookings = db.DeleteWhere<SunTableModel>("id = " + bookingId);
                scope.Complete();
                return bookings != 0 ? true : false;
            }
        }
        [HttpGet]
        public List<TimeSlotsViewModel> GetAvaliableTimeSlotByDate(string bookingDate, int bookingSize)
        {
            using (var scope = Current.ScopeProvider.CreateScope())
            {
                List<DateTime> slots = new List<DateTime>() { DateTime.Parse("16:00"), DateTime.Parse("18:00"), DateTime.Parse("20:00") };
                List<TimeSlotsViewModel> TimeSlots = new List<TimeSlotsViewModel>();
                foreach (var time in slots)
                {
                    TimeSlotsViewModel TimeSlot = new TimeSlotsViewModel
                    {
                        time = time,
                        bookedSeats = 10
                    };
                    TimeSlots.Add(TimeSlot);
                }
                
                var database = scope.Database;
                var data = database.Query<TimeSlotsViewModel>("SELECT bookingTime as Time, Sum(bookingSize) as bookedSeats FROM Bookings Where bookingDate = '" + bookingDate + "' Group By bookingTime");
                scope.Complete();
                foreach (TimeSlotsViewModel item in data)
                {
                    TimeSlots.Find(x => (x.time.ToString("H:mm") == item.time.ToString("H:mm"))).bookedSeats = (10 - item.bookedSeats - bookingSize);
                }
                return TimeSlots;
            }
        }
        [HttpGet]
        public IEnumerable<SunTableModel> GetAllBookings()
        {
            using (var scope = Current.ScopeProvider.CreateScope())
            {
                var database = scope.Database;
                var data = database.Query<SunTableModel>("SELECT * From Bookings Where bookingTime > DATEADD(day,-1,GETDATE()) Order by bookingTime");
                scope.Complete();
                return data;
            }
        }
        [HttpGet]
        public bool SendConfirmMail(string Email, int guestPageId)
        {
            var guestPageUrl = Umbraco.Content(guestPageId).Url;
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            string host = section.Network.Host;
            string user = section.Network.UserName;
            string password = section.Network.Password;
            int port = section.Network.Port;

            SmtpClient client = new SmtpClient
            {
                Port = port,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = host,
                Credentials = new System.Net.NetworkCredential(user, password)
            };
            MailMessage NotificationMail = new MailMessage("hello@masht.dk", Email);
            NotificationMail.IsBodyHtml = true;
            NotificationMail.Subject = "Bekræftelse";

            string path = HttpContext.Current.Server.MapPath("~/EmailTemplates/SubscribePageConfirmMail.html");
            string rootUrl = HttpContext.Current.Request.Url.Host;
            string content = System.IO.File.ReadAllText(path);

            var encrypted = Urlencrypto.GetEncryptedQueryString(Email);
            var link = rootUrl + guestPageUrl + "?q=" + encrypted;
            content = content.Replace("[confirmUrl]", link);
            NotificationMail.Body = content;

            client.Send(NotificationMail);
            return true;
        }
    }
}