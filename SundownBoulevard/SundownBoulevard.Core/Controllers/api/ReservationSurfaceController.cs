﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;
using Umbraco.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Scoping;
using Umbraco.Core.Persistence.DatabaseModelDefinitions;
using System;
using System.Web.Http;
using SundownBoulevard.Core.Models;
using Umbraco.Core.Composing;
using System.Collections.Generic;
using SundownBoulevard.Core.Helpers;
using NPoco;

namespace SundownBoulevard.Core.Controllers.api
{
    public class ReservationSurfaceController : SurfaceController
    {
        [System.Web.Http.HttpGet]
        public ActionResult GetBookingsByEmail(string cryptoEmail)
        {
            var decryptedEmail = Urlencrypto.GetDecryptedQueryString(cryptoEmail);
            if (!string.IsNullOrEmpty(decryptedEmail))
            {
                using (var scope = Current.ScopeProvider.CreateScope())
                {
                    var database = scope.Database;
                    IDatabase db = new Database(scope.Database.Connection);
                    // Get specific booking by id;
                    var data = database.Query<SunTableModel>("SELECT * From Bookings Where email = '" + decryptedEmail.Replace("@", "@@") + "'");
                    scope.Complete();
                    if (data != null)
                    {
                        return PartialView("~/Views/Partials/Reservation/ReservationList.cshtml", data);

                    }
                    else
                    {
                        return PartialView("~/Views/Partials/Reservation/Login.cshtml", data);
                    }
                }
            }
            else
            {
                return PartialView("~/Views/Partials/Reservation/Login.cshtml", null);
            }

        }
    }
}
