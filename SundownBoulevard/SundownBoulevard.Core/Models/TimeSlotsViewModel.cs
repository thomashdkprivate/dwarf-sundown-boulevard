using System;
using System.Collections.Generic;
using NPoco;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace SundownBoulevard.Core.Models
{
   
    public class TimeSlotsViewModel
    {
        public DateTime time { get; set; }
        public int bookedSeats { get; set; }
    }
}