using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace SundownBoulevard.Core.Models
{
    public class StatusViewModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public IMember member { get; set; }
        public bool isApproved { get; set; }
        public bool isNew { get; set; }
    }
}