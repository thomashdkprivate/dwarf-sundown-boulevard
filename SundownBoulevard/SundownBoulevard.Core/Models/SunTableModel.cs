﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace SundownBoulevard.Core.Models
{
    [TableName("Bookings")]
    [PrimaryKey("Id", AutoIncrement = true)]
    [ExplicitColumns]
    public class SunTableModel
    {
        [Column("id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        [Column("confirmed")]
        public string Confirmed { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("bookingSize")]
        public int BookingSize { get; set; }
        [Column("bookingDate")]
        public DateTime BookingDate { get; set; }
        [Column("bookingTime")]
        public DateTime BookingTime { get; set; }
        [Column("foodName")]
        public string FoodName { get; set; }
        [Column("drinkName")]
        public string DrinkName { get; set; }
    }
}
