﻿using SundownBoulevard.Core.Models;
using Umbraco.Core.Migrations;

namespace SundownBoulevard.Core
{
    public class SunMigrationPlan : MigrationPlan
    {
        public SunMigrationPlan()
            : base("Bookings")
        {
            From(string.Empty)
                .To<MigrationCreateTables>("first-migration");
        }
    }

    public class MigrationCreateTables : MigrationBase
    {
        public MigrationCreateTables(IMigrationContext context)
            : base(context)
        {

        }

        public override void Migrate()
        {
            if (!TableExists("Bookings"))
            {
                Create.Table<SunTableModel>().Do();
            }
        }
    }
}
