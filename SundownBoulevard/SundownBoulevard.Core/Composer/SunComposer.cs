﻿using SundownBoulevard.Core.Component;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace SundownBoulevard.Core.Composer
{
    public class SunComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Components()
                .Append<SunComponent>();
        }
    }
}
