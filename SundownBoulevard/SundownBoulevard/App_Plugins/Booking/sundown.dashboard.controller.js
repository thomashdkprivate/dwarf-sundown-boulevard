
function dashboardController($interval, $scope, $filter, $http, userService, notificationsService, localizationService, $attrs) {
    var vm = this;
    vm.UserName = 'guest';
    vm.Bookings = [];
    var user = userService.getCurrentUser().then(function (user) {
        vm.UserName = user.name;
        getAllBookings();
    });
   
    function getAllBookings() {
        $http({
            method: 'GET',
            url: '/umbraco/api/Booking/GetAllBookings'
        }).then(function (response) {
            vm.Bookings = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.deleteBooking = function (bookingId) {
        $http({
            method: 'GET',
            url: '/umbraco/api/Booking/DeleteBooking?bookingId=' + bookingId
        }).then(function (response) {
            getAllBookings();
        }, function (error) {
            console.log(error);
        });
    };
}
angular.module("umbraco").controller("Sundown.DashboardController", dashboardController);