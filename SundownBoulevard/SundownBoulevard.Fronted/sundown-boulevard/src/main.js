import Vue from 'vue'
import App from './App.vue'
import axios from "axios";
import VueAxios from 'vue-axios'
import VCalendar from 'v-calendar';
import VueRouter from "vue-router";

Vue.use(VueRouter);
Vue.prototype.$localUrl = VUE_APP_APIURL;

// Use v-calendar & v-date-picker components
Vue.use(VCalendar, {
  locales: {
    dk: {
      firstDayOfWeek: 2
    }
  }
});
Vue.use(VueAxios, axios)

// Format the date
import moment from "moment";
Vue.filter("formatDate", function(value) {
    if (value) {
      return moment(String(value)).format("DD-MM-YYYY");
    }
});

Vue.filter("formatTime", function (value) {
  if (value) {
    return moment(String(value)).format("H:mm");
  }
});

const router = new VueRouter({
  routes: [
    { path: "/", name: "booking" }
  ]
});
new Vue({
  el: "#app",
  router,
  render: h => h(App)
});
